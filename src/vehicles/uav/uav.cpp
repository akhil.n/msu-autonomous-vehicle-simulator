/*
Non-Commercial License - Mississippi State University Autonomous Vehicle Software (MAVS)

ACKNOWLEDGEMENT:
Mississippi State University, Center for Advanced Vehicular Systems (CAVS)

*NOTICE*
Thank you for your interest in MAVS, we hope it serves you well!  We have a few small requests to ask of you that will help us continue to create innovative platforms:
-To share MAVS with a friend, please ask them to visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to register and download MAVS for free.
-Feel free to create derivative works for non-commercial purposes, i.e. academics, U.S. government, and industry research.  If commercial uses are intended, please contact us for a commercial license at otm@msstate.edu or jonathan.rudd@msstate.edu.
-Finally, please use the ACKNOWLEDGEMENT above in your derivative works.  This helps us both!

Please visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to view the full license, or email us if you have any questions.

Copyright 2018 (C) Mississippi State University
*/
#include <vehicles/uav/uav.h>

#include <algorithm>
#include <sstream>

#include <mavs_core/math/utils.h>

namespace mavs {
namespace vehicle {

Uav::Uav() {
	pos_ = glm::vec3(0.0f, 0.0f, 100.0f);
	heading_ = 0.0f;
	mass_ = 1.38f; //kg
	inertia_ = 0.4f*mass_*0.175f*0.175f; //kg m^2
	max_lift_ = 75.0f*9.81f; // Newtons
	max_thrust_ = 100.0f*9.81f; // Newtons
	max_twist_ = 1.25f*inertia_*9.81f; // ?
	current_state_.pose.position = pos_;
}

Uav::~Uav() {}

void Uav::Update(environment::Environment *env, float forward, float rotation, float elevation, float dt) {
	Update(env, forward, 0.0f, rotation, elevation, dt);
}
void Uav::Update(environment::Environment *env, float forward, float sideways, float rotation, float elevation, float dt) {
	float dt2 = dt * dt;
	// update the vertical position
	pos_.z = pos_.z + 0.5f*elevation*max_lift_*dt2 / mass_;

	// update the heading
	heading_ += 0.5f*rotation*max_twist_*dt2 / inertia_;

	// update the lateral position
	glm::vec2 lt(cos(heading_), sin(heading_));
	glm::vec2 ls(-sin(heading_), cos(heading_));
	pos_.x += 0.5f*max_thrust_ * (dt2 / mass_)*(forward*lt.x + sideways * ls.x);
	pos_.y += 0.5f*max_thrust_ * (dt2 / mass_)*(forward*lt.y + sideways * ls.y);

	// update the vehicle class variables
	current_state_.twist.angular.z = (heading_ - current_steering_radians_) / dt;
	current_steering_radians_ = heading_; 
	current_state_.twist.linear.x = (pos_.x - current_state_.pose.position.x)/dt; 
	current_state_.twist.linear.y = (pos_.y - current_state_.pose.position.y) / dt;
	current_state_.twist.linear.z = (pos_.z - current_state_.pose.position.z) / dt;
	current_state_.pose.position.x = pos_.x;
	current_state_.pose.position.y = pos_.y;
	current_state_.pose.position.z = pos_.z;
	current_state_.pose.quaternion = glm::quat((float)cos(0.5*heading_), 0.0, 0.0, (float)sin(0.5*heading_)); 

	local_sim_time_ = local_sim_time_ + dt;
	if (log_data_) WriteState();
	updated_ = true;
}

} //namespace vehicle
} //namespace mavs