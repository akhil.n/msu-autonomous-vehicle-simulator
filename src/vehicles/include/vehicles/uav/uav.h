/*
Non-Commercial License - Mississippi State University Autonomous Vehicle Software (MAVS)

ACKNOWLEDGEMENT:
Mississippi State University, Center for Advanced Vehicular Systems (CAVS)

*NOTICE*
Thank you for your interest in MAVS, we hope it serves you well!  We have a few small requests to ask of you that will help us continue to create innovative platforms:
-To share MAVS with a friend, please ask them to visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to register and download MAVS for free.
-Feel free to create derivative works for non-commercial purposes, i.e. academics, U.S. government, and industry research.  If commercial uses are intended, please contact us for a commercial license at otm@msstate.edu or jonathan.rudd@msstate.edu.
-Finally, please use the ACKNOWLEDGEMENT above in your derivative works.  This helps us both!

Please visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to view the full license, or email us if you have any questions.

Copyright 2018 (C) Mississippi State University
*/
/**
* \class UAV
*
* Class that contains simple UAV model - up,down, forward, backward, rotate 
*
* \author Chris Goodin
*
* \date 3/18/2020
*/
#ifndef MAVS_UAV_H
#define MAVS_UAV_H
#ifdef USE_MPI
#include <mpi.h>
#endif
#include <mavs_core/environment/environment.h>
#include <vehicles/vehicle.h>

namespace mavs {
namespace vehicle {

class Uav : public Vehicle {
public:
	/// Constructor
	Uav();

	/// Destructor
	~Uav();

	/**
	* Update function inherited from vehicle base class
	* \param forward Like a throttle from -1 to 1 [0=hover]
	* \param rotation Like a steering from -1 to 1 [0=hover]
	* \param elevation Like a vertical throttle from -1 to 1 [0=hover]
	*/
	void Update(environment::Environment *env, float forward, float rotation, float elevation, float dt);

	/**
	* Update function with ability to move side to side
	* \param forward Like a throttle from -1 to 1 [0=hover]
	* \param sideways Like a sideways throttle from -1 to 1 [0=hover]
	* \param rotation Like a steering from -1 to 1 [0=hover]
	* \param elevation Like a vertical throttle from -1 to 1 [0=hover]
	*/
	void Update(environment::Environment *env, float forward, float sideways, float rotation, float elevation, float dt);

	void SetMotorParams(float max_lift, float max_thrust, float max_twist) {
		max_lift_ = max_lift;
		max_thrust_ = max_thrust;
		max_twist_ = max_twist;
	}

	void SetInertialParams(float mass, float rotational_inertia) {
		mass_ = mass;
		inertia_ = rotational_inertia;
	}

private:
	// state variables
	glm::vec3 pos_;
	float heading_;

	// uav properties
	float max_lift_;
	float max_thrust_;
	float max_twist_;
	float mass_;
	float inertia_;
};

} //namespace vehicle
} //namespace mavs

#endif
