/*
Non-Commercial License - Mississippi State University Autonomous Vehicle Software (MAVS)

ACKNOWLEDGEMENT:
Mississippi State University, Center for Advanced Vehicular Systems (CAVS)

*NOTICE*
Thank you for your interest in MAVS, we hope it serves you well!  We have a few small requests to ask of you that will help us continue to create innovative platforms:
-To share MAVS with a friend, please ask them to visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to register and download MAVS for free.
-Feel free to create derivative works for non-commercial purposes, i.e. academics, U.S. government, and industry research.  If commercial uses are intended, please contact us for a commercial license at otm@msstate.edu or jonathan.rudd@msstate.edu.
-Finally, please use the ACKNOWLEDGEMENT above in your derivative works.  This helps us both!

Please visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to view the full license, or email us if you have any questions.

Copyright 2018 (C) Mississippi State University
*/
#ifdef USE_MPI
#include <mpi.h>
#endif

#include <iostream>
#include <mavs_core/math/utils.h>
#include <glm/gtx/quaternion.hpp>
#include <tinyfiledialogs.h>

namespace mavs{
namespace math {

//ray-triangle intersection routine, see
// https://en.wikipedia.org/wiki/Moller-Trumbore_intersection_algorithm
bool RayTriangleIntersection(glm::vec3 rayOrigin,
	glm::vec3 rayVector,
	glm::vec3 vertex0,
	glm::vec3 vertex1,
	glm::vec3 vertex2,
	glm::vec3& outIntersectionPoint) {
	const float EPSILON = 0.0000001f;
	//glm::vec3 vertex0 = inTriangle->vertex0;
	//glm::vec3 vertex1 = inTriangle->vertex1;
	//glm::vec3 vertex2 = inTriangle->vertex2;
	glm::vec3 edge1, edge2, h, s, q;
	float a, f, u, v;
	edge1 = vertex1 - vertex0;
	edge2 = vertex2 - vertex0;
	h = glm::cross(rayVector, edge2);
	a = glm::dot(edge1, h);
	if (a>-EPSILON && a<EPSILON) {
		return false;
	}
	f = 1.0f / a;
	s = rayOrigin - vertex0;
	u = f * (glm::dot(s, h));
	if (u<0.0f || u> 1.0f) {
		return false;
	}
	q = glm::cross(s, edge1);
	v = f * glm::dot(rayVector, q);
	if (v<0.0 || (u + v)> 1.0) {
		return false;
	}
	float t = f * glm::dot(edge2, q);
	if (t>EPSILON) {
		outIntersectionPoint = rayOrigin + rayVector * t;
		return true;
	}
	else {
		return false;
	}
}

void QuatToEulerAngle(const glm::dquat q, double &pitch, double &roll, double &yaw) {
	glm::dvec3 euler = glm::eulerAngles(q);
	roll = euler.x;
	pitch = euler.y;
	yaw = euler.z;
}

glm::dquat EulerToQuat(double pitch, double roll, double yaw) {
	glm::dquat q = glm::quat(glm::vec3(roll, pitch, yaw));
	return q;
}

glm::vec3 RodriguesRotation(glm::vec3 v, glm::vec3 k, float theta) {
	float c = (float)cos(theta);
	float s = (float)sin(theta);
	glm::vec3 v_rot = v * c + glm::cross(k, v)*s + k * glm::dot(k, v)*(1.0f - c);
	return v_rot;
}

} //namespace math

namespace utils{

std::string GetSaveFileName() {
	std::string fname = "output.bmp";
	char const * lTheSelectFileName;
	lTheSelectFileName = tinyfd_saveFileDialog("Select file to save", "output.bmp", 0, NULL, "Output file name");
	if (lTheSelectFileName) {
		fname = std::string(lTheSelectFileName);
	}
	return fname;
}

void SleepSeconds(double x){
#ifdef USE_MPI
  double t1 = MPI_Wtime();
  double s = (double) x;
  while (x>(MPI_Wtime()-t1)){}
#endif
  return;
}

std::string GetPathFromFile(const std::string& path) {
	size_t found = path.find_last_of("/\\");
	std::string folder = path.substr(0, found);
	return folder;
}

std::string GetFileExtension(const std::string& file) {
	size_t found = file.find_last_of(".");
	return file.substr(found + 1);
}

/*
* Erase First Occurrence of given  substring from main string.
*/
void EraseSubString(std::string & mainStr, const std::string & toErase){
	// Search for the substring in string
	size_t pos = mainStr.find(toErase);
	if (pos != std::string::npos){
		// If found then erase it from string
		mainStr.erase(pos, toErase.length());
	}
}

} // namespace utils
} // namespace mavs
