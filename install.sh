#!/bin/sh

git submodule update --init --recursive

echo "Building Embree as sub-build..."

mkdir extern/embree/build
cd extern/embree/build
cmake .. "$@"
cmake --build . -j 8
echo "Installing Embree..."
make install

echo "Building MAVS..."
mkdir ../../../build
cd ../../../build
cmake .. "$@"
cmake --build . -j 8
echo "Installing MAVS..."
make install
