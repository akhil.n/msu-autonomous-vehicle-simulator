/*
Non-Commercial License - Mississippi State University Autonomous Vehicle Software (MAVS)

ACKNOWLEDGEMENT:
Mississippi State University, Center for Advanced Vehicular Systems (CAVS)

*NOTICE*
Thank you for your interest in MAVS, we hope it serves you well!  We have a few small requests to ask of you that will help us continue to create innovative platforms:
-To share MAVS with a friend, please ask them to visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to register and download MAVS for free.
-Feel free to create derivative works for non-commercial purposes, i.e. academics, U.S. government, and industry research.  If commercial uses are intended, please contact us for a commercial license at otm@msstate.edu or jonathan.rudd@msstate.edu.
-Finally, please use the ACKNOWLEDGEMENT above in your derivative works.  This helps us both!

Please visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to view the full license, or email us if you have any questions.

Copyright 2018 (C) Mississippi State University
*/
/**
* \file rp3d_driving_example.cpp
*
* Drive a MAVS vehicle implemented in RP3D
*
* Usage: >./rp3d_driving_example mavs_scene_file.json rp3d_vehicle_file.json surface_type cone_index pose_log_freq
*
* mavs_scene_file.json is a MAVS scene file, examples of which can
* be found in mavs/data/scenes.
*
* rp3d_vehicle_file.json examples can be found in mavs/data/vehicles/rp3d_vehicles
*
* Press "L" to start/stop logging poses and "P" to save the logged poses to a .vprp file.
*
* \author Chris Goodin
*
* \date 9/11/2019
*/
#include <iostream>
#include <stdlib.h>
#include <mavs_core/math/utils.h>
#include <vehicles/rp3d_veh/mavs_rp3d_veh.h>
#include <sensors/mavs_sensors.h>
#ifdef USE_OMP
#include <omp.h>
#endif
#ifdef USE_EMBREE
#include <raytracers/embree_tracer/embree_tracer.h>
#endif

int main(int argc, char *argv[]) {
	int myid = 0;
	int numprocs = 1;
#ifdef USE_MPI
	int ierr = MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
#endif
#ifdef USE_EMBREE
	if (argc < 2) {
		std::cerr << "ERROR, must provide scene file and vehicle file as arguments" << std::endl;
		return 1;
	}
	std::string scene_file(argv[1]);
	std::string vehic_file(argv[2]);

	std::string surface_type = "dry";
	float RCI = 6894.76*250.0f;
	if (argc > 3) {
		surface_type = std::string(argv[3]);
	}
	if (argc > 4) {
		RCI = 6894.76f*(float)atof(argv[4]);
	}
	float pose_log_freq = 0.0f;
	if (argc > 5) {
		pose_log_freq = (float)atof(argv[5]);
	}
	std::cout << "Scene File = " << scene_file << std::endl;
	mavs::raytracer::embree::EmbreeTracer scene;
	scene.Load(scene_file);
	std::cout << "Loaded scene with " << scene.GetNumberTrianglesLoaded() << " triangles. " << std::endl;
	std::cout << "Vehicle file = " << vehic_file << std::endl;
	std::cout << "Surface type and strength = " << surface_type << " " << RCI/ 6894.76f << std::endl;
#else
	std::cerr << "The Driving Example can only be run with Embree enabled" << std::endl;
	return 0;
#endif

	mavs::environment::Environment env;
	env.SetGlobalSurfaceProperties(surface_type, RCI);
	env.SetRaytracer(&scene);

	glm::vec3 sensor_offset(-10.0f, 0.0f, 1.5f);
	glm::quat sensor_orient(1.0f, 0.0f, 0.0f, 0.0f);
	glm::vec3 position(0.0f, 0.0f, 1.0f);
	glm::quat orient(1.0f, 0.0f, 0.0f, 0.0f);
	mavs::sensor::camera::RgbCamera camera;
	camera.SetEnvironmentProperties(&env);
	camera.Initialize(384, 384, 0.0035f, 0.0035f, 0.0035f);
	camera.SetRelativePose(sensor_offset, sensor_orient);
	camera.SetName("camera");
	camera.SetPose(position, orient);
	camera.SetElectronics(0.5f, 1.0f);
	
	mavs::vehicle::Rp3dVehicle veh;
	veh.Load(vehic_file);
	float zstart = scene.GetSurfaceHeight(0.0f, 0.0f);
	veh.SetPosition(0.0f,0.0f, zstart + 1.0f);
	veh.SetOrientation(1.0f, 0.0f, 0.0f, 0.0f);
	//veh.SetPosition(20.0f, -20.0f, 2.5f);
	//veh.SetOrientation(0.7071f, 0.0f, 0.0f, 0.7071f);
	float current_time = 0.0f;
	float dt = 1.0f / 30.0f;
	mavs::Waypoints logged_wp;
	int pose_log_steps = (int)ceil((1.0f/ dt)/pose_log_freq);
	bool logging = false;
	int nsteps = 0;
	std::vector<bool> driving_commands;

	if (myid == 0)camera.Display();
	while(camera.DisplayOpen()){
#ifdef USE_OMP
		double t1 = omp_get_wtime();
#endif
		current_time += dt;

		double throttle = 0.0;
		double steering = 0.0;
		double braking = 0.0;
		driving_commands = camera.GetKeyCommands();
		if (driving_commands[0]) {
			throttle = 1.0;
		}
		else if (driving_commands[1]) {
			braking = 1.0;
		}
		if (driving_commands[2]) {
			steering = 1.0;
		}
		else if (driving_commands[3]) {
			steering = -1.0;
		}

		if (camera.GetDisplay()->is_keyL()) {
			logging = !logging;
			if (logging) { std::cout << "Logging on. " << std::endl; }
			else { std::cout << "Logging off. " << std::endl; }
		}
		if (camera.GetDisplay()->is_keyP()) {
			logged_wp.SaveAsVprp("logged.vprp");
			break;
		}
	
		veh.Update(&env, (float)throttle, (float)steering, (float)braking, dt);
		
		mavs::VehicleState veh_state = veh.GetState();

		if (std::isinf(veh_state.pose.position.x) || std::isnan(veh_state.pose.position.x)) break;
		camera.SetPose(veh_state);

		if (nsteps%pose_log_steps == 0 && logging) {
			logged_wp.AddPoint(glm::vec2(veh_state.pose.position.x, veh_state.pose.position.y));
		}

		camera.Update(&env, dt);
		if (myid==0)camera.Display();
		nsteps++;

#ifdef USE_OMP
		double dwall = omp_get_wtime() - t1;
		if (dwall < dt) {
			int msleep = (int)(1000 * (dt - dwall));
			mavs::utils::sleep_milliseconds(msleep);
		}
#endif
	}

#ifdef USE_MPI
	MPI_Finalize();
#endif
	return 0;
}
