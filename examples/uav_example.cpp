/*
Non-Commercial License - Mississippi State University Autonomous Vehicle Software (MAVS)

ACKNOWLEDGEMENT:
Mississippi State University, Center for Advanced Vehicular Systems (CAVS)

*NOTICE*
Thank you for your interest in MAVS, we hope it serves you well!  We have a few small requests to ask of you that will help us continue to create innovative platforms:
-To share MAVS with a friend, please ask them to visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to register and download MAVS for free.
-Feel free to create derivative works for non-commercial purposes, i.e. academics, U.S. government, and industry research.  If commercial uses are intended, please contact us for a commercial license at otm@msstate.edu or jonathan.rudd@msstate.edu.
-Finally, please use the ACKNOWLEDGEMENT above in your derivative works.  This helps us both!

Please visit https://gitlab.com/cgoodin/msu-autonomous-vehicle-simulator to view the full license, or email us if you have any questions.

Copyright 2018 (C) Mississippi State University
*/
/**
* \file uav_example.cpp
*
* Fly a MAVS uav 
*
* Usage: >./uav_example mavs_scene_file.json
*
* mavs_scene_file.json is a MAVS scene file, examples of which can
* be found in mavs/data/scenes.
*
* \author Chris Goodin
*
* \date 3/18/2020
*/
#include <iostream>
#include <stdlib.h>
#include <mavs_core/math/utils.h>
#include "vehicles/uav/uav.h"
#include <sensors/mavs_sensors.h>
#ifdef USE_OMP
#include <omp.h>
#endif
#ifdef USE_EMBREE
#include <raytracers/embree_tracer/embree_tracer.h>
#endif

static void GetFlyingCommands(cimg_library::CImgDisplay* disp, float &throttle, float &side_throttle, float &rotation, float &elevation);

int main(int argc, char *argv[]) {
#ifdef USE_EMBREE
	if (argc < 2) {
		std::cerr << "ERROR, must provide scene file and vehicle file as arguments" << std::endl;
		return 1;
	}
	std::string scene_file(argv[1]);
	mavs::raytracer::embree::EmbreeTracer scene;
	scene.Load(scene_file);
#else
	std::cerr << "The UAV Example can only be run with Embree enabled" << std::endl;
	return 0;
#endif

	mavs::environment::Environment env;
	env.SetRaytracer(&scene);

	glm::vec3 sensor_offset(0.0f, 0.0f, 0.0f);
	glm::quat sensor_orient(0.7071f, 0.0f, 0.7071f, 0.0f);
	mavs::sensor::camera::RgbCamera camera;
	camera.SetEnvironmentProperties(&env);
	camera.Initialize(384, 384, 0.0035f, 0.0035f, 0.0035f);
	camera.SetRelativePose(sensor_offset, sensor_orient);
	camera.SetName("camera");
	camera.SetElectronics(0.5f, 1.0f);

	mavs::sensor::camera::RgbCamera front_camera;
	front_camera.SetEnvironmentProperties(&env);
	front_camera.Initialize(256, 256, 0.0035f, 0.0035f, 0.0035f);
	front_camera.SetRelativePose(sensor_offset, glm::quat(1.0f, 0.0f, 0.0f, 0.0f));
	front_camera.SetName("front_camera");
	front_camera.SetElectronics(0.5f, 1.0f);

	mavs::vehicle::Uav uav;

	float throttle = 0.0f;
	float side_throttle = 0.0f;
	float rotation = 0.0f;
	float elevation = 0.0f;

	float current_time = 0.0f;
	float dt = 1.0f / 30.0f;
	int nsteps = 0;
	while (true) {
#ifdef USE_OMP
		double t1 = omp_get_wtime();
#endif
		current_time += dt;
		GetFlyingCommands(camera.GetDisplay(), throttle, side_throttle, rotation, elevation);
		uav.Update(&env, throttle, side_throttle, rotation, elevation, dt);
		//env.SetActorPosition(0, veh_state.pose.position, veh_state.pose.quaternion);
		mavs::VehicleState veh_state = uav.GetState();
		camera.SetPose(veh_state);
		camera.Update(&env, dt);
		camera.Display();
		front_camera.SetPose(veh_state);
		front_camera.Update(&env, dt);
		front_camera.Display();
		nsteps++;
#ifdef USE_OMP
		double dwall = omp_get_wtime() - t1;
		if (dwall < dt) {
			int msleep = (int)(1000 * (dt - dwall));
			mavs::utils::sleep_milliseconds(msleep);
		}
#endif
	}

	return 0;
}

static void GetFlyingCommands(cimg_library::CImgDisplay* disp, float &throttle, float &side_throttle, float &rotation, float &elevation) {
	throttle = 0.0f;
	side_throttle = 0.0f;
	rotation = 0.0f;
	elevation = 0.0f;
	if (disp->is_keyW()) {
		elevation = 1.0f;
	}
	if (disp->is_keyS()) {
		elevation = -1.0f;
	}
	if (disp->is_keyA()) {
		rotation = 1.0f;
	}
	if (disp->is_keyD()) {
		rotation = -1.0f;
	}
	if (disp->is_keyARROWLEFT()) {
		side_throttle = 1.0f;
	}
	if (disp->is_keyARROWRIGHT()) {
		side_throttle = -1.0f;
	}
	if (disp->is_keyARROWUP()) {
		throttle = 1.0f;
	}
	if (disp->is_keyARROWDOWN()) {
		throttle = -1.0f;
	}
}